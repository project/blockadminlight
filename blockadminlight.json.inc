<?php


function _blockadminlight_json($themename) {

  module_load_include('inc', 'blockadminlight', 'blockadminlight.plugins');
  
  $debug = arg(6) && TRUE;
  $blocks_by_module = array();
  $q = db_query("SELECT * FROM {blocks} WHERE theme = '%s'", $themename);
  while ($row = db_fetch_object($q)) {
    $blocks_by_module[$row->module][$row->delta] = $row;
  }
  
  $blocks_json = array();
  $module_conf_json = array();
  foreach ($blocks_by_module as $module => $blocks_in_module) {
    foreach ($blocks_in_module as $delta => $block) {
      switch ($block->visibility) {
        case 1:
          $visibility_text = t('Show on listed pages:');
          break;
        case 2:
          $visibility_text = t('Show if PHP returns true:');
          break;
        case 0:
        default:
          $visibility_text = t('Hide on listed pages:');
      }
      $blocks_json[$module][$delta] = array(
        'info' => $block->info,
        'pages' => $block->pages,
        'visibility' => $block->visibility,
        'visibility_text' => $visibility_text,
      );
    }
    if (!empty($blocks_json[$module])) {
      if (function_exists($function = $module . '_blockadminlight')) {
        $module_conf_json[$module] = $function($blocks_json[$module]);
      }
    }
  }
  
  if ($debug) {
    dpm($blocks_by_module);
    dpm($blocks_json);
    return 'x';
  }
  else {
    drupal_json(compact('blocks_json', 'module_conf_json'));
    exit();
  }
}



