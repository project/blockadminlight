<?php


function _blockadminlight_page($themename) {
  global $theme_key, $custom_theme, $theme;
  $theme = NULL;

  // If non-default theme configuration has been selected, set the custom theme.
  $custom_theme = isset($themename) ? $themename : variable_get('theme_default', 'garland');

  module_load_include('inc', 'block', 'block.admin');
  
  // Fetch and sort blocks
  $blocks = _block_rehash();
  usort($blocks, '_block_compare');
  
  return drupal_get_form('blockadminlight_form', $blocks, $themename);
}


function _blockadminlight_require_library_path($libname, $module = NULL) {
  foreach (array($confpath = conf_path(), 'sites/all') as $site_dir) {
    if (is_file($path = "$site_dir/libraries/$libname")) {
      return $path;
    }
  }
  if (isset($module)) {
    $modulepath = drupal_get_path('module', $module);
    if (is_file($path = "$modulepath/$libname")) {
      return $path;
    }
  }
  $href = 'http://plugins.jquery.com/project/jCaret';
  $msg = <<<EOT
    <p>blockadminlight.module works better in combination with the <a href="$href">$libname</a> plugin. You can place this file in<br/>
    sites/all/libraries/$libname (recommended), or in<br/>
    $confpath/libraries/$libname, or in<br/>
    $modulepath/$libname (discouraged).
EOT;
  drupal_set_message($msg, 'warning');
}


function blockadminlight_form(&$form_state, $blocks, $themename) {
  global $theme_key, $custom_theme, $theme;
  
  if ($lib_path = _blockadminlight_require_library_path('jquery.caret.js', 'blockadminlight')) {
    drupal_add_js($lib_path);
    drupal_add_js(drupal_get_path('module', 'blockadminlight') . '/blockadminlight.js');
  }
  drupal_add_css(drupal_get_path('module', 'blockadminlight') . '/blockadminlight.css');

  // If non-default theme configuration has been selected, set the custom theme.
  $custom_theme = isset($themename) ? $themename : variable_get('theme_default', 'garland');
  init_theme();
  
  foreach (array('theme', 'theme_key', 'custom_theme') as $varname) {
    if ($themename != $$varname) {
      return array(
        'intro' => array(
          'value' => "Theme names mismatch: themename = $themename, $varname = $$varname.",
        ),
      );
    }
  }

  $throttle = module_exists('throttle');
  $block_regions = system_region_list($themename) + array(BLOCK_REGION_NONE => t('Disabled'));

  // Weights range from -delta to +delta, so delta should be at least half
  // of the amount of blocks present. This makes sure all blocks in the same
  // region get an unique weight.
  $weight_delta = round(count($blocks) / 2);
  
  $intro = <<<EOT
    <p>Sort blocks in theme '$themename'.<br/>
    Every text row represents a block. Rows starting with '-----' represent theme regions.<br/>
    Any deleted/missing row, and any row with an invalid region name will re-appear in the "disabled" region.</p>
EOT;

  // Build form tree
  $form = array(
    '#action' => url("admin/build/block/list-light/$themename"),
    '#tree' => TRUE,
    '#_regions' => $block_regions,
    '#_themename' => $themename,
    'intro' => array(
      '#value' => $intro,
    ),
  );
  
  $key_lengths = array();
  foreach ($blocks as $i => $block) {
    $key = $block['module'] . '.' . $block['delta'];
    $key_lengths[] = strlen($key);
  }
  $ideal_length = _blockadminlight_find_ideal_length($key_lengths);
  
  $weights_sorted = array();
  $block_rows_sorted = array();
  $blocks_keyed = array();
  $blocks_by_module = array();
  foreach ($blocks as $i => $block) {
    $region = $block['region'];
    if (!isset($block_regions[$region])) {
      $region = BLOCK_REGION_NONE;
    }
    $module = $block['module'];
    $delta = $block['delta'];
    $blocks_by_module[$module][$delta] = $block;
    $key = "$module.$delta";
    $blocks_keyed[$key] = $block;
    $row = "$key";
    if ($throttle && isset($block['throttle'])) {
      $row .= ' :throttle';
    }
    while (strlen($row) < $ideal_length) {
      $row .= ' ';
    }
    $block_info = $block['info'];
    $row = "$row - $block_info";
    $block_rows_sorted[$region][$key] = $row;
    $weights_sorted[$region][$key] = ($region != BLOCK_REGION_NONE) ? $block['weight'] : $block_info;
  }
  
  $text = '';
  foreach ($block_regions as $region => $region_title) {
    $text .= "\n\n--------------------------- $region ($region_title) ---------\n\n";
    if (is_array($block_rows_sorted[$region])) {
      array_multisort($weights_sorted[$region], $block_rows_sorted[$region]);
      if ($region != BLOCK_REGION_NONE) {
        foreach ($block_rows_sorted[$region] as $key => $row) {
          $text .= "$row\n";
        }
      }
      else {
        $region_blocks_by_module = array();
        foreach ($block_rows_sorted[$region] as $key => $row) {
          if (preg_match('/^([^\.]+)\.(.+)$/', $key, $m)) {
            list(,$module,$delta) = $m;
          }
          else {
            $module = '';
          }
          $region_blocks_by_module[$module][$delta] = $row;
        }
        ksort($region_blocks_by_module);
        foreach ($region_blocks_by_module as $module => $blocks_in_module) {
          ksort($blocks_in_module);
          foreach ($blocks_in_module as $delta => $row) {
            $text .= "$row\n";
          }
          $text .= "\n";
        }
      }
    }
  }
  
  $form['themename'] = array(
    '#type' => 'hidden',
    '#value' => $themename,
  );
  
  $form['blocks'] = array(
    '#type' => 'textarea',
    '#rows' => 26,
    '#default_value' => $text,
    '#attributes' => array(
      'wrap' => 'off',
      'spellcheck' => 'false',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save blocks'),
  );
  
  /*
  $form['blocks_json'] = array(
    '#type' => 'hidden',
    '#value' => '(' . json_encode($blocks_json) . ')',
  );
  
  $form['module_conf_json'] = array(
    '#type' => 'hidden',
    '#value' => '(' . json_encode($module_conf_json) . ')',
  );
  */
  
  $form['#_blocks'] = $blocks_keyed;

  return $form;
}


function _blockadminlight_find_ideal_length(array $key_lengths, $factor = 30) {
  sort($key_lengths, SORT_NUMERIC);
  $n = count($key_lengths);
  $length = 0;
  $best_length = 0;
  $cost = $n * $factor;
  $best_cost = $cost;
  for ($i=0; $i<$n; ++$i) {
    $increment = $key_lengths[$i] - $length;
    $length = $key_lengths[$i];
    $cost += $i * $increment;
    $cost -= $factor;
    if ($cost < $best_cost) {
      $best_cost = $cost;
      $best_length = $length;
    }
  }
  return $best_length;
}


/**
 * Process main blocks administration form submission.
 */
function blockadminlight_form_submit($form, &$form_state) {
  $regions = $form['#_regions'];
  $blocks = $form['#_blocks'];
  $themename = $form['#_themename'];
  $region = BLOCK_REGION_NONE;
  $status = FALSE;
  $weights = array();
  foreach (explode("\n", $form_state['values']['blocks']) as $row) {
    if (!trim($row)) {
      // ignore empty rows
    }
    if (preg_match('/^[=-]+ *([\d\w_-]+)/', $row, $m)) {
      // this is a region
      list(,$region) = $m;
      if (!isset($regions[$region])) {
        $region = BLOCK_REGION_NONE;
      }
      $status = (!empty($region) && $region != BLOCK_REGION_NONE);
      $region_db = $status ? $region : '';
    }
    else if (preg_match('/^([\d\w_-]+)\.([\.\d\w_-]+) *(\:throttle|)/', $row, $m)) {
      // this is a block
      list(,$module,$delta,$throttle) = $m;
      $throttle = $throttle ? 1 : 0;
      $key = "$module.$delta";
      if ($block = $blocks[$key]) {
        unset($blocks[$key]);
        $weight = $status ? ++$weights[$region] : 0;
        db_query("
          UPDATE {blocks} SET
            status = %d,
            weight = %d,
            region = '%s',
            throttle = %d
          WHERE
            module = '%s' AND
            delta = '%s' AND
            theme = '%s'
          ", $status, $weight, $region_db, $throttle, $module, $delta, $block['theme']
        );
      }
    }
  }
  
  // missing blocks go into the "disabled" region.
  $status = FALSE;
  $weight = 0;
  $region = BLOCK_REGION_NONE;
  $region_db = '';
  foreach ($blocks as $key => $block) {
    $throttle = isset($block['throttle']) ? $block['throttle'] : 0;
    $module = $block['module'];
    $delta = $block['delta'];
    if ($themename == $block['theme']) {
      db_query("
        UPDATE {blocks} SET
          status = %d,
          weight = %d,
          region = '%s',
          throttle = %d
        WHERE
          module = '%s' AND
          delta = '%s' AND
          theme = '%s'
        ", $status, $weight, $region_db, $throttle, $module, $delta, $themename
      );
    }
    else {
      drupal_set_message("Theme names mismatch in block $module.$delta: block[theme] == $block[theme]. Won't save this block.");
    }
  }
  
  drupal_set_message(t('The block settings have been updated.'));
  cache_clear_all();
}


