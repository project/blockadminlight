<?php


/**
 * Implementation of hook_blockadminlight in the name of block module.
 */
function block_blockadminlight(&$blocks) {
  $q = db_query("SELECT format, name FROM {filter_formats}");
  $formats = array();
  while ($row = db_fetch_object($q)) {
    $formats[$row->format] = $row->name;
  }
  $json_formats = json_encode($formats);
  $q = db_query("SELECT bid, body, format FROM {boxes}");
  while ($row = db_fetch_object($q)) {
    if (isset($blocks[$row->bid])) {
      $blocks[$row->bid]['body'] = substr(trim($row->body), 0, 100);
      $blocks[$row->bid]['format'] = $row->format;
    }
  }
  return <<<EOT
    (function(){
      var formats = $json_formats;
      return function(block, pluginHandler) {
        var format = formats[block.format];
        // $('<div>').val(block.body).appendTo(panel);
        var panel = $('<div>');
        $('<textarea readonly="readonly" rows="6">').val(block.body).appendTo(panel);
        $('<div class="format">').text(format).appendTo(panel);
        pluginHandler.prependToPanel(panel);
        pluginHandler.setDeleteLink('admin/build/block/delete/' + block.delta, 'Delete block');
      };
    })();
EOT;
}


/**
 * Implementation of hook_blockadminlight in the name of menu_block module.
 */
function menu_block_blockadminlight(&$blocks) {
  foreach (variable_get('menu_block_ids', array()) AS $delta) {
    if (isset($blocks[$delta])) {
      list($menu_name, $parent_mlid) = split(':', variable_get("menu_block_{$delta}_parent", ':0'));
      if ($menu_name) {
        $blocks[$delta]['menu_name'] = $menu_name;
      }
    }
  }
  $poweredit = module_exists('menu_editor') ? '/poweredit' : '';
  return <<<EOT
    (function(){
      return function(block, pluginHandler) {
        pluginHandler.setDeleteLink('admin/build/block/delete-menu-block/' + block.delta, 'Delete menu block');
        if (block.menu_name) {
          pluginHandler.setExtraLink('admin/build/menu-customize/' + block.menu_name + '$poweredit', 'Edit menu');
        }
      };
    })();
EOT;
}


/**
 * Implementation of hook_blockadminlight in the name of menu module.
 */
function menu_blockadminlight(&$blocks) {
  $poweredit = module_exists('menu_editor') ? '/poweredit' : '';
  return <<<EOT
    (function(){
      return function(block, pluginHandler) {
        pluginHandler.setExtraLink('admin/build/menu-customize/' + block.delta + '$poweredit', 'Edit menu');
      };
    })();
EOT;
}


/**
 * Implementation of hook_blockadminlight in the name of views module.
 */
function views_blockadminlight(&$blocks) {
  return <<<EOT
    (function(){
      return function(block, pluginHandler) {
        var view_name = block.delta.split('-').slice(0,-1).join('-');
        var view_display_name = block.delta.split('-').pop();
        pluginHandler.setExtraLink('admin/build/views/edit/' + view_name + '#views-tab-' + view_display_name, 'Edit view');
      };
    })();
EOT;
}



