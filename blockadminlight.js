
$(function(){
  var $f = $('#blockadminlight-form');
  var $t = $('textarea', $f);
  var $m = $('<div id="blockadminlight-monitor" class=""><span class="info">Configure selected block:</span> </div>');
  var $panel_root = $('<span id="blockadminlight-panel-root" class="hidden">info</span>').appendTo($m);
  var $panel = $('<div id="blockadminlight-panel" class="hidden">..</div>').appendTo($panel_root);
  var $a = $('<a>').appendTo($m);
  var $a_extra = $('<a id="blockadminlight-extra" class="hidden">..</a>').appendTo($m);
  var $a_del = $('<a id="blockadminlight-delete" class="hidden">delete</a>').appendTo($m);
  var themename = $('#edit-themename', $f).val();
  
  var panel_sticky = false;
  $panel_root.click(function(){
    if (panel_sticky = !panel_sticky) {
      $panel_root.addClass('sticky');
    } else {
      $panel_root.removeClass('sticky');
    }
  });
  
  // var blocks_json = eval($('#edit-blocks-json', $f).val());
  // var module_conf_json = eval($('#edit-module-conf-json', $f).val());
  
  var blocks_json = {};
  var module_conf_json = {};
  
  var test = $('<textarea style="position:absolute;top:0;left:0;visibility:hidden;"></textarea>').val("\n\n").appendTo($f);
  var testpos = test.caret(6,6).caret().start;
  test.remove();
  
  $m.appendTo('#blockadminlight-form');
  var url_dest = 'admin/build/block/list-light/' + themename;
  var base = $f.attr('action').split('admin/build/block').slice(0,-1).join('admin/build/block');
  $.ajax({
    url: base + "admin/build/block/list-light/" + themename + "/json",
    dataType:'json',
    success: function(data){
      blocks_json = data.blocks_json;
      for (var module in data.module_conf_json) {
        module_conf_json[module] = eval(data.module_conf_json[module]);
      }
    },
  });
  
  var update = function() {
    var position = $t.caret().start;
    var text = $t.val();
    if (testpos == 2) {
      // Firefox
      text = "\n" + text;
    }
    else if (testpos == 4) {
      // Opera
      text = text.split("\n").join(" \n");
    }
    var module = '';
    var delta = '';
    var desc = '';
    for (var pos0=position; pos0 >= 0 && text[pos0] != "\n"; --pos0) {}
    ++pos0;
    for (var pos=pos0; pos < text.length && text[pos] != " " && text[pos] != "\n" && text[pos] != "."; ++pos) {
      module += text[pos];
    }
    if (text[pos] == '.') {
      ++pos;
      for (; pos < text.length && text[pos] != " " && text[pos] != "\n"; ++pos) {
        delta += text[pos];
      }
    }
    if (text[pos] == ' ') {
      ++pos;
      for (; pos < text.length && text[pos] != "\n"; ++pos) {
        desc += text[pos];
      }
    }
    if (module.length && delta.length && blocks_json[module]) {
      var block = blocks_json[module][delta];
      if (block && typeof block == 'object') {
        block.module = module;
        block.delta = delta;
        $a.text(module + '.' + delta + ' ' + desc);
        $a.attr('href', base + 'admin/build/block/configure/' + module + '/' + delta + '?destination=' + url_dest);
        $m.addClass('enabled');
        var f = module_conf_json[module];
        var show_panel = false;
        $panel.html('');
        var pluginHandlerExec;
        var pluginHandler = new (function(){
          var _del = null;
          var _extraLink = null;
          this.setDeleteLink = function(path, text) {
            text = text ? text : 'Delete';
            _del = {href:base + path, text:text};
          }
          this.setExtraLink = function(path, text) {
            _extraLink = {href:base + path, text:text};
          }
          this.prependToPanel = function(content){
            $panel.prepend(content);
          }
          pluginHandlerExec = function(){
            if (_del) {
              $a_del.attr('href', _del.href);
              $a_del.html(_del.text);
              $a_del.removeClass('hidden');
            } else {
              $a_del.addClass('hidden');
            }
            if (_extraLink) {
              $a_extra.attr('href', _extraLink.href);
              $a_extra.html(_extraLink.text);
              $a_extra.removeClass('hidden');
            } else {
              $a_extra.addClass('hidden');
            }
          };
        })();
        if (block.pages.trim().length || block.visibility) {
          $('<div>').text(block.visibility_text + '').appendTo($panel);
          $('<textarea rows="4" readonly="readonly">').val(block.pages).appendTo($panel);
          show_panel = true;
        }
        if (f && typeof f == 'function') {
          f(block, pluginHandler);
          show_panel = true;
        }
        pluginHandlerExec();
        if (show_panel) {
          $panel_root.removeClass('hidden');
        }
        else {
          $panel_root.addClass('hidden');
        }
      }
    }
    else {
      $m.removeClass('enabled');
    }
  };
  $t.keyup(update).mouseup(update);
});
